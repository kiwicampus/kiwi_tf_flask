import requests, cv2, time, json, time
import numpy as np
import os
from PIL import Image
import pprint
import datetime, argparse


parser = argparse.ArgumentParser()
parser.add_argument('-c', '--camera', dest='camera', type=int,
                    default=0, help='rate of node')
args = parser.parse_args()


HEADERS = {'Accept': 'application/json', 'kiwibot-id': '666'}
UPLOAD_URL = 'http://34.234.80.14/api/v2/upload/'
UPLOAD_URL = 'http://vision.kiwicampus.com/api/v2/upload/'
UPLOAD_URL_DEBUG = 'http://127.0.0.1:5000/api/v2/upload/'
video = cv2.VideoCapture(args.camera)
quality = 80

start_time = time.time()

try:
    while True:
        # import pdb; pdb.set_trace()
        ret, image = video.read()


        if (time.time() - start_time) >= 0.1:

            _, buff = cv2.imencode(".jpg", image, [cv2.IMWRITE_JPEG_QUALITY, quality])

            files = {'file': buff.tostring()  }

            print('Showing image')
            cv2.imshow('send_image', image)
            cv2.waitKey(1)
            init = time.time()

            print("Posting image ...")
            # r = requests.post(UPLOAD_URL, data = files, headers=HEADERS)
            r = requests.post(UPLOAD_URL, files = files, headers=HEADERS)
            # print(r.text)

            print('Status code of the response: ')
            print(r.status_code)

            print("Posted! Response: ")
            response = r.json() #returns a dicto
            # print(response)


            pp = pprint.PrettyPrinter(indent=2)
            pp.pprint(response)
            print(time.time()-init)


            print(
                datetime.datetime.fromtimestamp(
                    int(response['meta']['timestamp'])
                ).strftime('%Y-%m-%d %H:%M:%S')
            )

            start_time = time.time()

except KeyboardInterrupt as e:
    print("Error:", e)
    video.release()
