import numpy as np
import json
import flask
# JSON dumper for complex ndarrays

class NumpyEncoder(json.JSONEncoder):
    def default(self, obj):
        """If input object is an ndarray it will be converted into a dict
        holding dtype, shape and the data, base64 encoded.
        """
        if isinstance(obj, np.ndarray):
            if obj.flags['C_CONTIGUOUS']:
                obj_data = obj.data
            else:
                cont_obj = np.ascontiguousarray(obj)
                assert(cont_obj.flags['C_CONTIGUOUS'])
                obj_data = cont_obj.data
            data_b64 = base64.b64encode(obj_data)
            return dict(__ndarray__= data_b64.decode('utf-8'),
                        dtype=str(obj.dtype),
                        shape=obj.shape)

def json_numpy_obj_hook(dct):
    """Decodes a previously encoded numpy ndarray with proper shape and dtype.

    :param dct: (dict) json encoded ndarray
    :return: (ndarray) if input was an encoded ndarray
    """
    if isinstance(dct, dict) and '__ndarray__' in dct:
        data = base64.b64decode(dct['__ndarray__'])
        return np.frombuffer(data, dct['dtype']).reshape(dct['shape'])
    return dct

# Helper code for json response
def dict_creator(boxes, scores, classes, num):
    dumped_boxes         = json.dumps(boxes,   cls = NumpyEncoder)
    dumped_scores        = json.dumps(scores,  cls = NumpyEncoder)
    dumped_shape         = json.dumps(classes, cls = NumpyEncoder)
    dumped_numdetections = json.dumps(num,     cls = NumpyEncoder)
    dict_to_return = {'boxes': dumped_boxes, 'scores': dumped_scores, 
                      'shape': dumped_shape, 'num_detections': dumped_numdetections }
    return dict_to_return

@app.route('/api/v1/json_detection/<filename>')
def json_detection(filename):
    _, image_np_expanded = image_preprocessing(filename)
    (boxes, scores, classes, num) = sess.run(
       [detection_boxes, detection_scores, detection_classes, num_detections],
       feed_dict={image_tensor: image_np_expanded})
    dict_resp = dict_creator(boxes, scores, classes, num)
    return jsonify(dict_resp)


