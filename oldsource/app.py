# app.py

# Imports from object detection
import numpy as np
import os
import six.moves.urllib as urllib
import sys
import tensorflow as tf

# Imports from flask example
import logging
import random
import time

# Froms from object detection
from collections import defaultdict
from PIL import Image

# From from flask example
from flask import Flask, jsonify, request, send_file

# Env Setup
sys.path.append("..")

# Recall to add utils folders and file for visualization utils
from utils import label_map_util
from utils import visualization_utils as vis_util


app = Flask(__name__)
app.config.from_object(__name__)

# Constant & Globals from detection object
MODEL_NAME = 'ssd_mobilenet_v1_coco_11_06_2017'
PATH_TO_CKPT = MODEL_NAME + '/frozen_inference_graph.pb'
PATH_TO_LABELS = os.path.join('data', 'mscoco_label_map.pbtxt')
NUM_CLASSES = 90

app.config['JSON_AS_ASCII'] = False

# Graph loader and all kind of stuff
detection_graph = tf.Graph()
with detection_graph.as_default():
    od_graph_def = tf.GraphDef()
    with tf.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
        serialized_graph = fid.read()
        od_graph_def.ParseFromString(serialized_graph)
        tf.import_graph_def(od_graph_def, name='')

# Loading label map
label_map = label_map_util.load_labelmap(PATH_TO_LABELS)
categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=NUM_CLASSES, use_display_name=True) # TODO: Review map to categories
category_index = label_map_util.create_category_index(categories)

# Helper code
def load_image_into_numpy_array(image):
    (im_width, im_height) = image.size
    return np.array(image.getdata()).reshape(
            (im_height, im_width, 3)).astype(np.uint8)

# Enforce in the object detection graph no new nodes
detection_graph.finalize()

sess = tf.Session(graph=detection_graph)

# Definite input and output Tensors for detection_graph
image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')
# Each box represents a part of the image where a particular object was detected.
detection_boxes = detection_graph.get_tensor_by_name('detection_boxes:0')
# Each score represent how level of confidence for each of the objects.
# Score is shown on the result image, together with the class label.
detection_scores = detection_graph.get_tensor_by_name('detection_scores:0')
detection_classes = detection_graph.get_tensor_by_name('detection_classes:0')
num_detections = detection_graph.get_tensor_by_name('num_detections:0')


@app.route('/')
def classify():

    file_path = request.args['file_path']
    app.logger.info("Classifying image %s" % (file_path),)

    image = Image.open(file_path)
    # the array based representation of the image will be used later in order to prepare the
    # result image with boxes and labels on it.
    image_np = load_image_into_numpy_array(image)
    # Expand dimensions since the model expects images to have shape: [1, None, None, 3]
    image_np_expanded = np.expand_dims(image_np, axis=0)
    t = time.time()
    # Actual detection.
    (boxes, scores, classes, num) = sess.run(
        [detection_boxes, detection_scores, detection_classes, num_detections],
        feed_dict={image_tensor: image_np_expanded})
    #time detection
    dt = time.time() - t
    app.logger.info("Execution time: %0.2f" % (dt * 1000.))
    vis_util.visualize_boxes_and_labels_on_image_array(
        image_np,
        np.squeeze(boxes),
        np.squeeze(classes).astype(np.int32),
        np.squeeze(scores),
        category_index,
        use_normalized_coordinates=True,
        line_thickness=8)
    result_str = 'result.png'
    vis_util.save_image_array_as_png(image_np, result_str)
    app.logger.info("Image %s was classified" % file_path)
    return send_file(result_str, mimetype='image/png')

if __name__ == '__main__':

    app.run(debug=False, port=8009)
