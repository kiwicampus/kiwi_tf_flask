# detection_app.py

# Imports from object detection
import numpy as np
import os
import sys
import tensorflow as tf
import cv2
import collections

#Bringing Cristian to object detection
from name import network_name, model_path
from tfinterface.supervised import SupervisedInputs
from model import Model


# Imports from flask example
import random
import time

# Froms from object detection
from PIL import Image

# From from flask example
from flask import Flask, jsonify, request, send_file, redirect, url_for, send_from_directory, render_template
from werkzeug.utils import secure_filename
from werkzeug.contrib.fixers import ProxyFix

# Imports from the json library
import base64
import json
import requests

# Env Setup
sys.path.append("..")

# Recall to add utils folders and file for visualization utils
from utils import label_map_util
from utils import visualization_utils as vis_util

# Secure upload files services
app = Flask(__name__)
app.wsgi_app = ProxyFix(app.wsgi_app)
app.config.update(
        SECRET_KEY = b'\xeb!_\xc5\xf5\x9b\xcf\xbe\xf4\x8e\x90\x05\xf2\xdf\xb7/M\xa4\x8acLN\xbf\xc1'
        )

# Constant & Globals from detection object
MODEL_NAME = 'faster_rcnn_inception_resnet_v2_atrous_coco_11_06_2017'
MODEL_NAME = 'ssd_mobilenet_v1_coco_11_06_2017'
PATH_TO_CKPT = MODEL_NAME + '/frozen_inference_graph.pb'
PATH_TO_LABELS = os.path.join('data', 'mscoco_label_map.pbtxt')
NUM_CLASSES = 90
CATEGORIES = ['walk_precaution', 'red', 'green', 'yellow','walk_stop','walk_allow','off']
OFFSET = 2
URL_WEBHOOK = 'https://kiwibot-server-staging.herokuapp.com/v2/bots/{}/detections'
# Constants & Parameters from flask service
UPLOAD_FOLDER = os.path.join(os.path.expanduser('~'), 'images')
ALLOWED_EXTENSIONS = set(['jpg','jpeg','png'])

# App configurations
app.config['JSON_AS_ASCII'] = False
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

# Flask helper functions

def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in  ALLOWED_EXTENSIONS

# Check if UPLOAD_FOLDER does exists
if not os.path.exists(UPLOAD_FOLDER):
    os.mkdir(UPLOAD_FOLDER)

# Graph loader and all kind of stuff
detection_graph = tf.Graph()
with detection_graph.as_default():
    od_graph_def = tf.GraphDef()
    with tf.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
        serialized_graph = fid.read()
        od_graph_def.ParseFromString(serialized_graph)
        tf.import_graph_def(od_graph_def, name='')

# Loading label map
label_map = label_map_util.load_labelmap(PATH_TO_LABELS)
categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=NUM_CLASSES, use_display_name=True)
category_index = label_map_util.create_category_index(categories)

# Helper code
def load_image_into_numpy_array(image):
    if not (image.mode == "RGB"):
        image = image.convert("RGB")
    return np.array(image)

# Helper code for image response
def image_preprocessing(filename):
    image = Image.open(os.path.join(UPLOAD_FOLDER, filename))
    image_np = load_image_into_numpy_array(image)
    # Expand dimensions since the model expects images to have shape: [1, None, None, 3]
    return image_np, np.expand_dims(image_np, axis=0)

# Enforce in the object detection graph no new nodes
# detection_graph.finalize()

sess = tf.Session(graph=detection_graph)

# Definite input and output Tensors for detection_graph
image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')
# Each box represents a part of the image where a particular object was detected.
detection_boxes = detection_graph.get_tensor_by_name('detection_boxes:0')
# Each score represent how level of confidence for each of the objects.
# Score is shown on the result image, together with the class label.
detection_scores = detection_graph.get_tensor_by_name('detection_scores:0')
detection_classes = detection_graph.get_tensor_by_name('detection_classes:0')
num_detections = detection_graph.get_tensor_by_name('num_detections:0')

###### Traffic Light MODEL ############
## seed: resultados repetibles
seed = 31
np.random.seed(seed=seed)
random.seed(seed)


# inputs
inputs = SupervisedInputs(
    name = network_name + "_inputs",
    graph = detection_graph,
    sess = sess,
    # tensors
    features = dict(shape = (None, 64, 64, 3)),
    labels = dict(shape = (None,), dtype = tf.uint8)
)

template = Model(
    n_classes  = 7,
    name       = network_name,
    model_path = model_path,
    graph      = detection_graph,
    sess       = sess
)

inputs = inputs()
model = template(inputs)

model.initialize(restore = True)

# JSON helper functions

def category_checker(category):
    CHECKERS = ["person", "bicycle", "car", "motorcycle", "bus", "truck", "fire hydrant", "traffic light"]
    return True if category in CHECKERS else False

def dict_detection_generator(image,
                            boxes,
                            classes,
                            scores,
                            category_index,
                            inference_time,
                            max_boxes_to_report = 50,
                            min_score_thresh = .5
                            ):
    """Create dictonary to hold values that are going to be send as a response to a JSON"""
    IMG_WIDTH = image.shape[1]
    IMG_HEIGHT = image.shape[0]
    box_to_class_name_str = collections.defaultdict(list)
    box_to_detection_percent = collections.defaultdict(list)
    image_dict = {"width": IMG_WIDTH, "height": IMG_HEIGHT}
    meta_dict = {"inferenceTime": inference_time, "timestamp": time.time()}
    main_dict = {"image" : image_dict, "meta" : meta_dict}
    if not max_boxes_to_report:
        max_boxes_to_report = boxes.shape[0]
    for i in range(min(max_boxes_to_report, boxes.shape[0])):
        if scores is None or scores[i] > min_score_thresh:
            box = tuple(boxes[i].tolist())
            if classes[i] in category_index.keys():
                class_name = category_index[classes[i]]['name']
            else:
                class_name = "Object without name"
            detection_trust = int(100 * scores[i])
            box_to_class_name_str[box] = class_name
            box_to_detection_percent[box] = detection_trust
    categories_list = []
    for box in  box_to_class_name_str:
        category = box_to_class_name_str[box]
        ymin, xmin, ymax, xmax = box
        box_dict = {"xmin" : xmin*IMG_WIDTH, "ymin": ymin*IMG_HEIGHT,
                   "xmax" : xmax*IMG_WIDTH, "ymax": ymax*IMG_HEIGHT}
        category_dict = {"category": category, "box": box_dict }
        if category_checker(category):
            if category == 'traffic light':
                (xmin, xmax) = (int(xmin*IMG_WIDTH)+OFFSET, int(xmax*IMG_WIDTH)-OFFSET)
                (ymin, ymax) = (int(ymin*(IMG_HEIGHT))+OFFSET, int(ymax*(IMG_HEIGHT))-OFFSET)
                cropped = image[ymin:ymax+1,xmin:xmax+1]
                cropped = cv2.resize(cropped,(64,64))
                predictions = model.predict(features=[cropped])
                label = np.argmax(predictions, axis=1)
                light = CATEGORIES[label[0]]
                prob = int(predictions[0,label[0]]*100)
                meta_dict = {"state": light, "estimator": prob}
                category_dict["meta"] = meta_dict
            else:
                mid_x = (xmax + xmin) / 2
                mid_y = (ymax + ymin) / 2
                apx_distance = round((1 - (xmax - xmin))**4, 1)
                meta_dict = {"distance" : apx_distance}
                meta_dict["warning"] = False
                if apx_distance <= 0.5:
                    if mid_x > 0.3 and mid_x < 0.7:
                        meta_dict["warning"] = True
                category_dict["meta"] = meta_dict
        category_dict["estimator"] = box_to_detection_percent[box]
        categories_list.append(category_dict)
    main_dict["detections"] = categories_list
    return main_dict

def img_detection_generator(image,
                            boxes,
                            classes,
                            scores,
                            category_index,
                            max_boxes_to_report = 150,
                            min_score_thresh = .5,
                            traffic_lights_show = True,
                            distances_show = True
                            ):
    """Create image to send a processed image that is goinf to be a response to a JSON"""
    IMG_WIDTH = image.shape[1]
    IMG_HEIGHT = image.shape[0]
    box_to_class_name_str = collections.defaultdict(list)
    box_to_detection_percent = collections.defaultdict(list)
    if not max_boxes_to_report:
        max_boxes_to_report = boxes.shape[0]
    for i in range(min(max_boxes_to_report, boxes.shape[0])):
        if scores is None or scores[i] > min_score_thresh:
            box = tuple(boxes[i].tolist())
            if classes[i] in category_index.keys():
                class_name = category_index[classes[i]]['name']
            else:
                class_name = "Object without name"
            detection_trust = int(100 * scores[i])
            box_to_class_name_str[box] = class_name
            box_to_detection_percent[box] = detection_trust
    for box in  box_to_class_name_str:
        category = box_to_class_name_str[box]
        ymin, xmin, ymax, xmax = box
        box_dict = {"xmin" : xmin*IMG_WIDTH, "ymin": ymin*IMG_HEIGHT,
                   "xmax" : xmax*IMG_WIDTH, "ymax": ymax*IMG_HEIGHT}
        category_dict = {"category": category, "box": box_dict }
        if category_checker(category):
            if (category == 'traffic light') & traffic_lights_show:
                (xmin, xmax) = (int(xmin*IMG_WIDTH)+OFFSET, int(xmax*IMG_WIDTH)-OFFSET)
                (ymin, ymax) = (int(ymin*(IMG_HEIGHT))+OFFSET, int(ymax*(IMG_HEIGHT))-OFFSET)
                cropped = image[ymin:ymax+1,xmin:xmax+1]
                cropped = cv2.resize(cropped,(64,64))
                predictions = model.predict(features=[cropped])
                label = np.argmax(predictions, axis=1)
                light = CATEGORIES[label[0]]
                prob = int(predictions[0,label[0]]*100)
                cv2.putText(image, light+' {0:.2f}%'.format(prob), (xmin, ymin + 10), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (0, 0, 255), 2)
            elif (distances_show):
                mid_x = (xmax + xmin) / 2
                mid_y = (ymax + ymin) / 2
                apx_distance = round((1 - (xmax - xmin))**4, 1)
                cv2.putText(image, '{} d'.format(apx_distance), (int(mid_x*IMG_WIDTH),int(mid_y*IMG_HEIGHT)),  cv2.FONT_HERSHEY_SIMPLEX, 0.7, (255,255,255), 2)
                if apx_distance <= 0.5:
                    if mid_x > 0.3 and mid_x < 0.7:
                        cv2.putText(image, 'WARNING!!!', (50,50), cv2.FONT_HERSHEY_SIMPLEX, 1.0, (0,0,255), 3)
    return image

@app.route('/api/v1/upload/', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        # Check if the post request has the file part.
        if 'file' not in request.files:
            # print('No file part')
            return redirect(request.url)
        file = request.files['file']
        # if user does not select file, browser also
        # submit a empty part without filename
        if file.filename == '':
            # print('No selected file')
            return redirect(request.url)

        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))

            if request.headers['Accept'] == 'application/json':
                return redirect(url_for('json_warning_response', filename= filename))

            if request.headers['Accept'] == 'image':
                return redirect(url_for('image_warning_response', filename = filename))

            return redirect(url_for('image_warning_response', filename = filename))
    return '''
    <!doctype html>
    <title>Upload new File</title>
    <h1>Upload new File</h1>
    <form method=post enctype=multipart/form-data>
    <p><input type=file name=file>
    <input type=submit value=Upload>
    </form>
    '''
@app.route('/api/v2/upload/', methods=['POST'])
def upload_file_v2_post():
    # Check if the post request has the file part.
    # import pdb; pdb.set_trace()
    if 'file' not in request.files:
        # print('No file part')
        return redirect(request.url)
    file = request.files['file']
    kiwibot_id = request.headers.get('kiwibot-id', 'Virtual_bot')
    # if user does not select file, browser also
    # submit a empty part without filename
    if file.filename == '':
        # print('No selected file')
        return redirect(request.url)
    if file:
        filename = secure_filename(file.filename)
        file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        # import pdb; pdb.set_trace()
        if request.headers['Accept'] == 'application/json':
            return redirect(url_for('json_warning_response', filename = filename, kiwibot_id = kiwibot_id))
        if request.headers['Accept'] == 'image':
            return redirect(url_for('image_warning_response', filename = filename))
        if request.form['type'] == 'trafic':
            return redirect(url_for('traffic_light_detector', filename = filename))
        if request.form['type'] == 'distance':
            return redirect(url_for('distance_estimator', filename = filename))
        else:
            return redirect(url_for('image_warning_response', filename = filename))

@app.route('/api/v2/upload/', methods=['GET'])
def upload_file_v2_get():
    return render_template('detection.html')

@app.route('/uploads/<filename>')
def uploaded_file(filename):
    print('type of the argument recived is: ' + str(type(filename)) + 'variable has value  ' +  filename)
    return send_from_directory(app.config['UPLOAD_FOLDER'], filename)


@app.route('/api/v1/image_detection/<filename>')
def img_detection(filename):
    image_np, image_np_expanded = image_preprocessing(filename)
    (boxes, scores, classes, num) = sess.run(
       [detection_boxes, detection_scores, detection_classes, num_detections],
       feed_dict={image_tensor: image_np_expanded})
    vis_util.visualize_boxes_and_labels_on_image_array(
        image_np,
        np.squeeze(boxes),
        np.squeeze(classes).astype(np.int32),
        np.squeeze(scores),
        category_index,
        use_normalized_coordinates=True,
        line_thickness=8)
    result_str = os.path.join(UPLOAD_FOLDER, "last_detection.png")
    vis_util.save_image_array_as_png(image_np, result_str)
    return send_file(result_str, mimetype='image/png')

@app.route('/api/v2/traffic_detector/<filename>')
def traffic_light_detector(filename):
    image_np, image_np_expanded = image_preprocessing(filename)
    (boxes, scores, classes, num) = sess.run(
       [detection_boxes, detection_scores, detection_classes, num_detections],
       feed_dict={image_tensor: image_np_expanded})
    image_np = img_detection_generator(image_np,
                                        np.squeeze(boxes),
                                        np.squeeze(classes).astype(np.int32),
                                        np.squeeze(scores),
                                        category_index,
                                        distances_show = False)
    result_str = os.path.join(UPLOAD_FOLDER, "last_detection.png")
    vis_util.save_image_array_as_png(image_np, result_str)
    return send_file(result_str, mimetype='image/png')

@app.route('/api/v2/distance_estimator/<filename>')
def distance_estimator(filename):
    image_np, image_np_expanded = image_preprocessing(filename)
    (boxes, scores, classes, num) = sess.run(
       [detection_boxes, detection_scores, detection_classes, num_detections],
       feed_dict={image_tensor: image_np_expanded})
    image_np = img_detection_generator(image_np,
                                        np.squeeze(boxes),
                                        np.squeeze(classes).astype(np.int32),
                                        np.squeeze(scores),
                                        category_index,
                                        traffic_lights_show = False)
    result_str = os.path.join(UPLOAD_FOLDER, "last_detection.png")
    vis_util.save_image_array_as_png(image_np, result_str)
    return send_file(result_str, mimetype='image/png')

@app.route('/')
def status():
    user = {'nickname': 'David'}
    return render_template('index.html',
                            title='Home',
                            user = user)

@app.route('/api/v2/json_warning_response/<filename>/kiwibot/<kiwibot_id>/')
def json_warning_response(filename, kiwibot_id = 'Virtual_bot'):
    image_np, image_np_expanded = image_preprocessing(filename)
    t = time.time()
    # Actual detection
    (boxes, scores, classes, num) = sess.run(
       [detection_boxes, detection_scores, detection_classes, num_detections],
       feed_dict={image_tensor: image_np_expanded})
    dt = time.time() - t
    dict_resp = dict_detection_generator(image_np,
                                        np.squeeze(boxes),
                                        np.squeeze(classes).astype(np.int32),
                                        np.squeeze(scores),
                                        category_index,
                                        dt)
    dict_resp['kiwibot-id'] = kiwibot_id
    url_kiwibot_webhook = URL_WEBHOOK.format("kiwibot"+kiwibot_id)
    r = requests.post(url_kiwibot_webhook, json=dict_resp)

    meta_server = dict(web_hook_response = r.status_code, host = url_kiwibot_webhook)
    dict_resp['meta_server'] = meta_server

    return jsonify(dict_resp)

@app.route('/api/v3/image_warning_response/<filename>')
def image_warning_response(filename):
    image_np, image_np_expanded = image_preprocessing(filename)
    (boxes, scores, classes, num) = sess.run(
       [detection_boxes, detection_scores, detection_classes, num_detections],
       feed_dict={image_tensor: image_np_expanded})
    image_np = img_detection_generator(image_np,
                                        np.squeeze(boxes),
                                        np.squeeze(classes).astype(np.int32),
                                        np.squeeze(scores),
                                        category_index)
    result_str = os.path.join(UPLOAD_FOLDER, "last_detection.png")
    vis_util.visualize_boxes_and_labels_on_image_array(
        image_np,
        np.squeeze(boxes),
        np.squeeze(classes).astype(np.int32),
        np.squeeze(scores),
        category_index,
        use_normalized_coordinates=True,
        line_thickness=8)
    vis_util.save_image_array_as_png(image_np, result_str)
    return send_file(result_str, mimetype='image/png')
