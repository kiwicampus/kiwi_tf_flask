Things to run the flask inference over the network
```bash
curl localhost:8009/?file_path=/home/dgromov/Documents/models/object_detection/test_images/image1.jpg
```
# Network Model Zoo
Recall to download and to check the name of the neural networks that are provided. Link with models: [zoo repo](https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/detection_model_zoo.md)


## Running the application

```
export FLASK_APP=detection_app.py
python -m flask run
```
